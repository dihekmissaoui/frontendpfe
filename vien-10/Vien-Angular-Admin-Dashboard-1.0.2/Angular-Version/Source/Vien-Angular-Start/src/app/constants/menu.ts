export interface IMenuItem {
  id?: string;
  icon?: string;
  label: string;
  to: string;
  newWindow?: boolean;
  subs?: IMenuItem[];
}

const data: IMenuItem[] = [
  {
    id: "vien",
    icon: "iconsminds-air-balloon-1",
    label: "menu.vien",
    to: "/app/vien",
    subs: [
      {
        icon: "simple-icon-paper-plane",
        label: "menu.start",
        to: "/app/vien/start",
      },

      {
        icon: "simple-icon-basket-loaded",
        label: "menu.reservation",
        to: "/app/vien/reservation",
      },
    ],
  },

  {
    id: "pages",
    icon: "iconsminds-digital-drawing",
    label: "menu.pages",
    to: "/app/pages",
    subs: [
      {
        id: "pages-authorization",
        label: "menu.authorization",
        to: "/user",
        subs: [
          {
            icon: "simple-icon-user-following",
            label: "menu.login",
            to: "/user/login",
            newWindow: true,
          },
          {
            icon: "simple-icon-user-follow",
            label: "menu.register",
            to: "/user/register",
            newWindow: true,
          },
          {
            icon: "simple-icon-user-unfollow",
            label: "menu.forgot-password",
            to: "/user/forgot-password",
            newWindow: true,
          },
          {
            icon: "simple-icon-user-following",
            label: "menu.reset-password",
            to: "/user/reset-password",
            newWindow: true,
          },
        ],
      },
    ],
  },

  {
    id: "Chalet",
    icon: "iconsminds-shop-4",
    label: "menu.chalet",
    to: "/app/chalets",
  },

  {
    id: "Reservation",
    icon: "iconsminds-shop-4",
    label: "menu.reservation",
    to: "/app/reservation",
    subs: [
      {
        icon: "simple-icon-paper-plane",
        label: "menu.image-list",
        to: "/app/reservation/image-list",
      },
      {
        icon: "simple-icon-book-open",
        label: "menu.details",
        to: "/app/reservation/details",
      },
    ],
  },
  {
    id: "second-menu",
    icon: "iconsminds-three-arrow-fork",
    label: "menu.second-menu",
    to: "/app/second-menu",
    subs: [
      {
        icon: "simple-icon-paper-plane",
        label: "menu.second",
        to: "/app/second-menu/second",
      },
    ],
  },
  {
    id: "blankpage",
    icon: "iconsminds-bucket",
    label: "menu.blank-page",
    to: "/app/blank-page",
  },
  {
    id: "docs",
    icon: "iconsminds-library",
    label: "menu.docs",
    to: "https://vien-docs.coloredstrategies.com/",
    newWindow: true,
  },
];
export default data;
