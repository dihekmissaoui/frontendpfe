import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListeChaletComponent } from './liste-chalet/liste-chalet.component';


const routes: Routes = [

  {
    path: '', component: ListeChaletComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChaletRoutingModule { }
