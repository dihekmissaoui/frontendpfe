import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { HotkeyModule, HotkeysService } from 'angular2-hotkeys';
import { AccordionModule, BsDropdownModule, ModalModule, PaginationModule, RatingModule, TabsModule } from 'ngx-bootstrap';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ComponentsCardsModule } from 'src/app/components/cards/components.cards.module';
import { ComponentsCarouselModule } from 'src/app/components/carousel/components.carousel.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ChaletRoutingModule } from './chalet-routing.module';
import { ListeChaletComponent } from './liste-chalet/liste-chalet.component';
import { ChaletService } from './services/chalet.service';
import { ListChaletResolverService } from './services/list-chalet-resolver.service';




@NgModule({
  declarations: [ListeChaletComponent],
  imports: [
    SharedModule,
    CommonModule,
    LayoutContainersModule,
    ChaletRoutingModule,
    PagesContainersModule,
    ComponentsCarouselModule,
    ComponentsCardsModule,
    ComponentsChartModule,
    RatingModule,
    FormsModuleAngular,
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    HotkeyModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true,
    })
  ],
  providers: [ChaletService, ListChaletResolverService ]
})
export class ChaletModule { }
