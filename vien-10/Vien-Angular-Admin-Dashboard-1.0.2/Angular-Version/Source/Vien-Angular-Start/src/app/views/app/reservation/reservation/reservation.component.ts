import { Component, OnInit } from '@angular/core';
import { Reservation } from '../../model/reservation.model';
import { ReservationService } from '../../services/reservation.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
 
  reservation: Reservation[];

  constructor(private reservationService : ReservationService) { }

  ngOnInit(): void {
    this.reservationService.listeReservation().subscribe(res => {
      console.log(res);
      this.reservation = res
    });
  }

}
