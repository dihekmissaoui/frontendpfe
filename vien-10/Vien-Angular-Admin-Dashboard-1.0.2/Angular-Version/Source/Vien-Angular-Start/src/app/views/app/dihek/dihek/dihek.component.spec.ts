import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DihekComponent } from './dihek.component';

describe('DihekComponent', () => {
  let component: DihekComponent;
  let fixture: ComponentFixture<DihekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DihekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DihekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
