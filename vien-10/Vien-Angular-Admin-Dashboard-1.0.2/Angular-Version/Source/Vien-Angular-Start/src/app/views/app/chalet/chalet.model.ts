export interface Chalet {
    id_chalet?: string,
    description?: string;
    prix?: number,
    nom?: string;
}