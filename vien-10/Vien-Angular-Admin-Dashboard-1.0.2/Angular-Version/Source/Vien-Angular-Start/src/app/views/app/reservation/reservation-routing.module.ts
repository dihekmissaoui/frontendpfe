import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { ImageListComponent } from './image-list/image-list.component';
import { ReservationComponent } from './reservation/reservation.component';


const routes: Routes = [
  {
    path: '', component: ReservationComponent,
  children:[
    { path: 'image-list', component: ImageListComponent },
    { path: 'details', component: DetailsComponent },
  ]
  }
  


  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationRoutingModule { }
