import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';

import { DihekRoutingModule } from './dihek-routing.module';
import { DihekComponent } from './dihek/dihek.component';

@NgModule({
  declarations: [DihekComponent],
  imports: [
    CommonModule,
    DihekRoutingModule,
    LayoutContainersModule
  ]
})
export class DihekModule { }
