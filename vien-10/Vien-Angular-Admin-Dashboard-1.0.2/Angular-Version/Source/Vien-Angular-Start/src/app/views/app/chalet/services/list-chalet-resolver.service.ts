import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { ChaletService } from './chalet.service';

@Injectable({
  providedIn: 'root'
})
export class ListChaletResolverService implements Resolve<any> {

  constructor(private chaletService: ChaletService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log('provider')
    return this.chaletService.getListChalets();
  }
}
