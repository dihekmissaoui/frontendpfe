import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { BlankPageComponent } from './blank-page/blank-page.component';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [BlankPageComponent, AppComponent  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    SharedModule,
    LayoutContainersModule,
    HttpClientModule,

   
   
  ]
})
export class AppModule { }

