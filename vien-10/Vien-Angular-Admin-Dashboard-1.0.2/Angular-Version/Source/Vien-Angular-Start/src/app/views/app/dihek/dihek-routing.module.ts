import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DihekComponent } from './dihek/dihek.component';


const routes: Routes = [
  {
    path: '', component: DihekComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DihekRoutingModule { }
