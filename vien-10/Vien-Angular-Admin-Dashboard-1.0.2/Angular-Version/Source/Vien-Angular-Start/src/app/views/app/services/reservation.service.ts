import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Reservation } from '../model/reservation.model';
const httpOptions = {
  headers: new HttpHeaders( {'Content-Type': 'application/json'} )
  };

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
 apiURL: string = 'http://localhost:8087/api/test';

  constructor(private http : HttpClient ) { }

listeReservation(): Observable<Reservation[]>{
return this.http.get<Reservation[]>(this.apiURL);
}











}
