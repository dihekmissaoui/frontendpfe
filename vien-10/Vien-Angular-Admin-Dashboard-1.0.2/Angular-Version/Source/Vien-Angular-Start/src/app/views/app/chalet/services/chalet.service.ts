import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Chalet } from '../chalet.model';

@Injectable({
  providedIn: 'root'
})
export class ChaletService {

  constructor(private httpClient: HttpClient) { }
  
  getListChalets():Observable<Chalet>{
    return this.httpClient.get(`${environment.apiUrlSB}/api/chalet`);
  }
}
    